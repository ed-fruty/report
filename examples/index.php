<?php
use Fruty\Reporter\Examples\Sources\ElasticSearchProfitSource;
use Fruty\Reporter\Examples\Sources\EloquentRegistrationsSource;
use Fruty\Reporter\Examples\Sources\RegistrationConversionSource;
use Fruty\Reporter\ReportBuilder;
use Fruty\Reporter\ReportCriteria;

require __DIR__ . '/../vendor/autoload.php';


$builder = new ReportBuilder();

$builder->getSourceCollection()->push(new ElasticSearchProfitSource());
$builder->getSourceCollection()->push(new EloquentRegistrationsSource());
$builder->getSourceCollection()->push(new RegistrationConversionSource());

$criteria = new ReportCriteria();
$criteria->setMetrics('profit', 'registrations', 'conversion');

$report = $builder->build($criteria);

var_dump($report->all());
///
/// [
///     'elastic' => [
///         'profit' => 23
///     ],
///     'conversion'    => [
///         'transformed_conversion'    => 4.37
///     ]
/// ]


/*
 * Integration with service locator
 *
 * $container->bind(ReportBuilderInterface::class, function() use ($container) {
 *      $builder = new ReportBuilder;
 *      $builder->getSourceCollection()->push(new SomeSource);
 *      $builder->getSourceCollection()->push($container['report.source.google.analytics']);
 * });
 *
 *
 */


/*
 * Integration with Laravel DI
 *
 * $container->afterResolving(ReportBuilderInterface::class, function(ReportBuilderInterface $builder) use ($container) {
 *      $builder->getSourceCollection()->push($container->make('yandex.stats.source'));
 *      $builder->getSourceCollection()->push(new SourceWithoutExternalDependencies);
 * });
 */