<?php

namespace Fruty\Reporter\Examples\Transformers;

use Fruty\Reporter\Contracts\TransformerInterface;

class ConversionTransformer implements TransformerInterface
{

    /**
     * @param mixed $data
     * @return mixed
     */
    public function transform($data)
    {
        return ['transformed_conversion' => round($data, 2)];
    }
}
