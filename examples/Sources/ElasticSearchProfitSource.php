<?php

namespace Fruty\Reporter\Examples\Sources;

use Fruty\Reporter\Contracts\MetricSourceInterface;
use Fruty\Reporter\Contracts\ReportCriteriaInterface;

class ElasticSearchProfitSource implements MetricSourceInterface
{

    /**
     * @return string
     */
    public function getName()
    {
        return 'elastic';
    }

    /**
     * @return array
     */
    public function getMetrics()
    {
        return [
            'profit'
        ];
    }

    /**
     * Check is supports by builder parameters.
     *
     * @param ReportCriteriaInterface $builder
     * @return bool
     */
    public function supports(ReportCriteriaInterface $builder)
    {
        foreach ($this->getMetrics() as $metric) {
            if ($builder->hasMetric($metric)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get data.
     *
     * @param ReportCriteriaInterface $builder
     * @return array
     */
    public function get(ReportCriteriaInterface $builder)
    {
        return [
            'profit'   => 23
        ];
    }
}
