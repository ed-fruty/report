<?php

namespace Fruty\Reporter\Examples\Sources;

use Fruty\Reporter\Contracts\MetricSourceInterface;
use Fruty\Reporter\Contracts\ReportCriteriaInterface;

class EloquentRegistrationsSource implements MetricSourceInterface
{
    const REGISTRATIONS = 'registrations';

    /**
     * @return string
     */
    public function getName()
    {
        return self::REGISTRATIONS;
    }

    /**
     * @return array
     */
    public function getMetrics()
    {
        return [self::REGISTRATIONS];
    }

    /**
     * Check is supports by builder parameters.
     *
     * @param ReportCriteriaInterface $builder
     * @return bool
     */
    public function supports(ReportCriteriaInterface $builder)
    {
        return $builder->hasMetric(self::REGISTRATIONS);
    }

    /**
     * Get data.
     *
     * @param ReportCriteriaInterface $builder
     * @return array|int
     */
    public function get(ReportCriteriaInterface $builder)
    {
        return [
            self::REGISTRATIONS => 19
        ];
    }
}
