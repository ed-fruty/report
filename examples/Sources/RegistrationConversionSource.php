<?php

namespace Fruty\Reporter\Examples\Sources;

use Fruty\Reporter\Contracts\MetricSourceInterface;
use Fruty\Reporter\Contracts\ReportCriteriaInterface;
use Fruty\Reporter\Contracts\ReportResultInterface;
use Fruty\Reporter\Contracts\SourceHasDependenciesInterface;
use Fruty\Reporter\Contracts\SourceHasTransformerInterface;
use Fruty\Reporter\Contracts\TransformerInterface;
use Fruty\Reporter\Examples\Transformers\ConversionTransformer;

class RegistrationConversionSource implements MetricSourceInterface, SourceHasDependenciesInterface, SourceHasTransformerInterface
{
    /**
     * @var ReportResultInterface
     */
    protected $result;

    /**
     * @return string
     */
    public function getName()
    {
        return 'conversion';
    }

    /**
     * @return array
     */
    public function getMetrics()
    {
        return (array) 'registration_conversion';
    }

    /**
     * Check is supports by builder parameters.
     *
     * @param ReportCriteriaInterface $builder
     * @return bool
     */
    public function supports(ReportCriteriaInterface $builder)
    {
        return true;
    }

    /**
     * Get data.
     *
     * @param ReportCriteriaInterface $builder
     * @return array
     */
    public function get(ReportCriteriaInterface $builder)
    {
        return $this->result->get('elastic')['profit'] * 0.19;
    }

    /**
     * @param ReportCriteriaInterface $builder
     * @return array
     */
    public function getDependencies(ReportCriteriaInterface $builder)
    {
        return [
            'elastic'   => ['profit']
        ];
    }

    /**
     * @param ReportCriteriaInterface $builder
     * @return TransformerInterface
     */
    public function getTransformer(ReportCriteriaInterface $builder)
    {
        return new ConversionTransformer();
    }

    /**
     * @param ReportResultInterface $result
     * @return mixed|void
     */
    public function setCurrentResult(ReportResultInterface $result)
    {
        $this->result = $result;
    }
}
