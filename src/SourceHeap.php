<?php

namespace Fruty\Reporter;

use Fruty\Reporter\Contracts\MetricSourceInterface;

class SourceHeap extends \SplHeap
{
    protected $sources = [];

    /**
     * @param MetricSourceInterface $source
     * @param array $dependencies
     * @param int $priority
     */
    public function addSource(MetricSourceInterface $source, array $dependencies = [], $priority = 0)
    {
        if (isset($this->sources[$source->getName()])) {
            $this->sources[$source->getName()]['priority'] = $priority > $this->sources[$source->getName()]['priority']
                ?   $priority
                :   $this->sources[$source->getName()]['priority'];
        } else {
            $this->sources[$source->getName()] = compact('source', 'dependencies', 'priority');
        }
    }


    public function complete()
    {
        array_map([$this, 'insert'], $this->sources);
        $this->sources = [];
    }

    /**
     * Compare elements in order to place them correctly in the heap while sifting up.
     * @link http://php.net/manual/en/splheap.compare.php
     * @param mixed $value1 <p>
     * The value of the first node being compared.
     * </p>
     * @param mixed $value2 <p>
     * The value of the second node being compared.
     * </p>
     * @return int Result of the comparison, positive integer if <i>value1</i> is greater than <i>value2</i>, 0 if they are equal, negative integer otherwise.
     * </p>
     * <p>
     * Having multiple elements with the same value in a Heap is not recommended. They will end up in an arbitrary relative position.
     * @since 5.3.0
     */
    protected function compare($value1, $value2)
    {
        return $value1['priority'] <=> $value2['priority'];
    }
}
