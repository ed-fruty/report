<?php

namespace Fruty\Reporter\Contracts;

interface SourceCollectionInterface
{
    /**
     * Push a new source.
     *
     * @param MetricSourceInterface $source
     */
    public function push(MetricSourceInterface $source);

    /**
     * Check is source exists.
     *
     * @param string $name
     * @return bool
     */
    public function has($name);

    /**
     * Remove source.
     *
     * @param string $name
     * @return bool
     */
    public function remove($name);

    /**
     * Get source instance.
     *
     * @param string $name
     * @return MetricSourceInterface
     */
    public function get($name);

    /**
     * Get source list
     * @return MetricSourceInterface[]|\Iterator
     */
    public function all();
}
