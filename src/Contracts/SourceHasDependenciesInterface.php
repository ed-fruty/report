<?php

namespace Fruty\Reporter\Contracts;

interface SourceHasDependenciesInterface
{
    /**
     * @param ReportCriteriaInterface $builder
     * @return array
     */
    public function getDependencies(ReportCriteriaInterface $builder);

    /**
     * @param ReportResultInterface $result
     * @return mixed
     */
    public function setCurrentResult(ReportResultInterface $result);
}
