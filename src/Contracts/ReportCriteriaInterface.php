<?php

namespace Fruty\Reporter\Contracts;

interface ReportCriteriaInterface
{
    /**
     * @param array ...$metrics
     */
    public function setMetrics(...$metrics);

    /**
     * @param string $metric
     */
    public function addMetric($metric);

    /**
     * @param array ...$metrics
     */
    public function addMetrics(...$metrics);

    /**
     * @param string $name
     * @return bool
     */
    public function hasMetric($name);

    /**
     * @param string $name
     */
    public function removeMetric($name);

    /**
     * @param array ...$filters
     */
    public function setFilters(...$filters);

    /**
     * @param string $name
     * @param mixed $value
     */
    public function addFilter($name, $value);

    /**
     * @param string $name
     * @return bool
     */
    public function hasFilter($name);

    /**
     * @param string $name
     */
    public function removeFilter($name);

    /**
     * @param string $format
     */
    public function setFormat($format);

    /**
     * @param string $aggregation
     */
    public function useAggregation($aggregation);
}
