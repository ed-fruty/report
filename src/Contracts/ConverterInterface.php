<?php

namespace Fruty\Reporter\Contracts;

interface ConverterInterface
{
    /**
     * @param ReportResultInterface $result
     * @return mixed
     */
    public function convert(ReportResultInterface $result);
}
