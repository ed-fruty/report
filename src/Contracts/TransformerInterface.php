<?php

namespace Fruty\Reporter\Contracts;

interface TransformerInterface
{
    /**
     * @param mixed $data
     * @return mixed
     */
    public function transform($data);
}
