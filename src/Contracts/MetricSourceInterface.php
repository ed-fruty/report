<?php

namespace Fruty\Reporter\Contracts;

interface MetricSourceInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return array
     */
    public function getMetrics();

    /**
     * Check is supports by builder parameters.
     *
     * @param ReportCriteriaInterface $builder
     * @return bool
     */
    public function supports(ReportCriteriaInterface $builder);

    /**
     * Get data.
     *
     * @param ReportCriteriaInterface $builder
     * @return array
     */
    public function get(ReportCriteriaInterface $builder);
}
