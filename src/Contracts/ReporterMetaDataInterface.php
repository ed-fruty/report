<?php

namespace Fruty\Reporter\Contracts;

interface ReporterMetaDataInterface extends \JsonSerializable
{
    public function toArray();
}
