<?php

namespace Fruty\Reporter\Contracts;

interface ReporterInterface
{
    /**
     * @param ReportCriteriaInterface $builder
     * @return mixed
     */
    public function build(ReportCriteriaInterface $builder);

    /**
     * @return SourceCollectionInterface
     */
    public function getSourceCollection();

    /**
     * @return ReporterMetaDataInterface
     */
    public function getMetaData();
}
