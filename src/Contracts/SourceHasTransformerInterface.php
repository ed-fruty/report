<?php

namespace Fruty\Reporter\Contracts;

interface SourceHasTransformerInterface
{
    /**
     * @param ReportCriteriaInterface $builder
     * @return TransformerInterface
     */
    public function getTransformer(ReportCriteriaInterface $builder);
}
