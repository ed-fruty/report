<?php

namespace Fruty\Reporter\Contracts;

interface ReportResultInterface extends \JsonSerializable
{
    /**
     * @param string $key
     * @param mixed $value
     */
    public function set($key, $value);

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key);

    /**
     * Get all keys.
     *
     * @return array
     */
    public function all();

    /**
     * @param $name
     * @return bool
     */
    public function has($name);

    /**
     * @return \ArrayIterator
     */
    public function getIterator();
}
