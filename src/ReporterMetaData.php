<?php

namespace Fruty\Reporter;

use Fruty\Reporter\Contracts\ReporterMetaDataInterface;
use Fruty\Reporter\Contracts\SourceCollectionInterface;

class ReporterMetaData implements ReporterMetaDataInterface
{
    /**
     * @var SourceCollectionInterface
     */
    private $sourceCollection;

    /**
     * RepoterMetaData constructor.
     * @param SourceCollectionInterface $sourceCollection
     */
    public function __construct($sourceCollection)
    {
        $this->sourceCollection = $sourceCollection;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->jsonSerialize();
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        $tree = [];
        $sources = [];
        $metrics = [];

        foreach ($this->sourceCollection->all() as $source) {
            $tree[$source->getName()] = $source->getMetrics();
            $sources[] = $source->getName();

            foreach ($source->getMetrics() as $metric) {
                $metrics[] = $metric;
            }
        }

        return compact('tree', 'sources', 'metrics');
    }
}
