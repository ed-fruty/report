<?php

namespace Fruty\Reporter;

use Fruty\Reporter\Contracts\MetricSourceInterface;
use Fruty\Reporter\Contracts\ReportCriteriaInterface;
use Fruty\Reporter\Contracts\ReporterInterface;
use Fruty\Reporter\Contracts\ReporterMetaDataInterface;
use Fruty\Reporter\Contracts\SourceCollectionInterface;
use Fruty\Reporter\Contracts\SourceHasDependenciesInterface;
use Fruty\Reporter\Contracts\SourceHasTransformerInterface;

/**
 * Class Reporter
 * @package Fruty\Reporter
 */
class ReportBuilder implements ReporterInterface
{

    /**
     * @var SourceCollectionInterface
     */
    private $sourceCollection;


    /**
     * Reporter constructor.
     * @param SourceCollectionInterface $sourceCollection
     */
    public function __construct(SourceCollectionInterface $sourceCollection = null)
    {
        $this->sourceCollection = $sourceCollection ? $sourceCollection : new SourceCollection();
    }

    /**
     * @param ReportCriteriaInterface $builder
     * @return ReportResult
     */
    public function build(ReportCriteriaInterface $builder)
    {
        $sources = $this->prepareSources($builder);

        return $this->loadResult($builder, $sources);
    }

    /**
     * @return SourceCollectionInterface
     */
    public function getSourceCollection()
    {
        return $this->sourceCollection;
    }

    /**
     * @return ReporterMetaDataInterface
     */
    public function getMetaData()
    {
        return new ReporterMetaData($this->getSourceCollection());
    }

    /**
     * @param ReportCriteriaInterface $builder
     * @return SourceHeap
     */
    protected function prepareSources(ReportCriteriaInterface $builder)
    {
        $heap = new SourceHeap();

        foreach ($this->getSourceCollection()->all() as $source) {
            if ($source->supports($builder)) {
                $priority = 1;
                $heap->addSource($source, $this->resolveSourceDependencies($builder, $source, $heap, $priority));
            }
        }

        $heap->complete();

        return $heap;
    }

    /**
     * @param ReportCriteriaInterface $builder
     * @param MetricSourceInterface $source
     * @param SourceHeap $heap
     * @param $priority
     * @return array
     */
    protected function resolveSourceDependencies(ReportCriteriaInterface $builder, MetricSourceInterface $source, SourceHeap $heap, $priority)
    {
        $dependencies = [];

        if (! $source instanceof SourceHasDependenciesInterface) {
            return $dependencies;
        }

        ++$priority;

        foreach ($source->getDependencies($builder) as $dependency => $metrics) {
            $dependencies[] = $dependency;
            $instance = $this->getSourceCollection()->get($dependency);

            $heap->addSource($instance, $this->resolveSourceDependencies($builder, $instance, $heap, $priority), $priority);

            foreach ((array) $metrics as $metric) {
                if (false === $builder->hasMetric($metric)) {
                    $builder->addMetric($metric);
                }
            }
        }

        return $dependencies;
    }

    /**
     * @param ReportCriteriaInterface $builder
     * @param SourceHeap $heap
     * @return ReportResult
     */
    protected function loadResult(ReportCriteriaInterface $builder, SourceHeap $heap)
    {
        $result = new ReportResult();

        foreach ($heap as $item) {
            /** @var MetricSourceInterface $source */
            $source = $item['source'];

            if ($source instanceof SourceHasDependenciesInterface) {
                $source->setCurrentResult($result);
            }

            $data = $source->get($builder);

            if ($source instanceof SourceHasTransformerInterface) {
                $data = $source->getTransformer($builder)->transform($data);
            }

            $result->set($source->getName(), $data);
        }

        return $result;
    }
}
