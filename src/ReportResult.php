<?php

namespace Fruty\Reporter;

use Fruty\Reporter\Contracts\ReportResultInterface;

class ReportResult implements ReportResultInterface
{
    protected $items = [];

    /**
     * @param $name
     * @param $value
     */
    public function set($name, $value)
    {
        $this->items[$name] = $value;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'items' => $this->items
        ];
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->items[$key];
    }

    /**
     * Get all keys.
     *
     * @return array
     */
    public function all()
    {
        return $this->items;
    }

    /**
     * @param $name
     * @return bool
     */
    public function has($name)
    {
        return isset($this->items[$name]);
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }
}
