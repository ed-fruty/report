<?php

namespace Fruty\Reporter;

use Fruty\Reporter\Contracts\ReportCriteriaInterface;

class ReportCriteria implements ReportCriteriaInterface
{
    protected $metrics = [];

    protected $filters = [];

    protected $format;

    protected $aggregation;

    /**
     * @param array ...$metrics
     */
    public function setMetrics(...$metrics)
    {
        $this->metrics = array_reverse($metrics);
    }

    /**
     * @param $name
     */
    public function addMetric($name)
    {
        $this->metrics[$name] = true;
    }

    /**
     * @param array ...$metrics
     */
    public function addMetrics(...$metrics)
    {
        $this->metrics = array_replace($this->metrics, array_reverse($metrics));
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasMetric($name)
    {
        return isset($this->metrics[$name]);
    }

    /**
     * @param $name
     */
    public function removeMetric($name)
    {
        unset($this->metrics[$name]);
    }

    /**
     * @param array ...$filters
     */
    public function setFilters(...$filters)
    {
        $this->filters = $filters;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function addFilter($name, $value)
    {
        $this->filters[$name] = $value;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasFilter($name)
    {
        return isset($this->filters[$name]);
    }

    /**
     * @param string $name
     */
    public function removeFilter($name)
    {
        unset($this->filters[$name]);
    }

    /**
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * @param string $aggregation
     */
    public function useAggregation($aggregation)
    {
        $this->aggregation = $aggregation;
    }
}
