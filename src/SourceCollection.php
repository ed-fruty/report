<?php

namespace Fruty\Reporter;

use Fruty\Reporter\Contracts\MetricSourceInterface;
use Fruty\Reporter\Contracts\SourceCollectionInterface;

class SourceCollection implements SourceCollectionInterface
{

    protected $sources = [];

    /**
     * Push a new source.
     *
     * @param MetricSourceInterface $source
     */
    public function push(MetricSourceInterface $source)
    {
        $this->sources[$source->getName()] = $source;
    }

    /**
     * Check is source exists.
     *
     * @param string $name
     * @return bool
     */
    public function has($name)
    {
        return isset($this->sources[$name]);
    }

    /**
     * Remove source.
     *
     * @param string $name
     * @return bool
     */
    public function remove($name)
    {
        unset($this->sources[$name]);
    }

    /**
     * Get source instance.
     *
     * @param string $name
     * @return MetricSourceInterface
     */
    public function get($name)
    {
        if (! isset($this->sources[$name])) {
            throw new \InvalidArgumentException(sprintf('Undefined source %s', $name));
        }

        return $this->sources[$name];
    }

    /**
     * Get source list
     * @return MetricSourceInterface[]|\Iterator
     */
    public function all()
    {
        return $this->sources;
    }
}
